import { Console } from "console";
import { reader } from "ionicons/icons";

export const getBase64 = (file:  File) => {
    return new Promise(resolve => {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
            let base64 = reader.result as String;
            resolve(base64);
        };
    });
};

export const addImage =(e: any,admin: String) => {
    let file = e.target.files[0];
    getBase64(file)
        .then(result => {
            let imageSrc = String(result);
        })
        .catch(error => {
            console.log(error);
        });
}