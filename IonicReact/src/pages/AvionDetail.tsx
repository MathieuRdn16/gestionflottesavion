import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar,IonCard, IonCardContent, IonImg,IonCardHeader, IonCardSubtitle, IonButton,IonCardTitle,IonButtons,IonMenuButton } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import useData from '../components/GetData';
import Token from '../components/GetToken';
import { useParams } from 'react-router';

const Deplacements: React.FC = () => {
  const {idAv}=useParams<{ idAv: string;}>();
  Token(idAv);

  const {data,error}=useData("http://localhost:8080/deplacements/"+idAv);
  var nom="";
  var img="";
  function connect(){
    window.window.location.href="/changeprofil/"+idAv;
  
  }
  if(data.length==0){
    return <h1>Chargement....</h1>
  }else{
    nom= data[0].avion.numero+" "+data[0].avion.categorie.categorie+" " +data[0].avion.nom ;
    img= data[0].avion.profil;
    console.log(data[0].avion.profil);
    return (<IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle> {nom}<IonImg src={img} />
            <IonButton  type="button"  >
              changer photo
            </IonButton>
          </IonTitle>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large"> {nom}</IonTitle>
          </IonToolbar>
        </IonHeader>
          {data.map(item => {
            return (
              <IonCard>
              <IonCardContent>
                <IonCardSubtitle>Date de déplacement: {item.datedep}</IonCardSubtitle>
                <IonCardSubtitle>Debut Km: {item.debutkm}</IonCardSubtitle>
                <IonCardSubtitle>Fin Km: {item.finkm}</IonCardSubtitle>
              </IonCardContent>
              </IonCard>
            )
          })}
      </IonContent>
    </IonPage>
    );
  }
  
};

export default Deplacements;
