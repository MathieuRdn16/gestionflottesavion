import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar,IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonButtons,IonMenuButton,IonItem } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import useData from '../components/GetData';

const Avions: React.FC = () => {
  const {data,error}=useData("http://localhost:8080/avions");
  if(!data){
    return <h1>Chargement....</h1>
  }else{
    return (<IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Liste des Avions</IonTitle>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Avions</IonTitle>
          </IonToolbar>
        </IonHeader>
          {data.map(item => {
            return (
              <IonCard>
              <IonItem routerLink={`/deplacement/${item.idavion}`} detail={false}>
              <IonCardContent>
                <IonCardTitle>{item.categorie.categorie}</IonCardTitle>
                <IonCardContent>{item.nom}</IonCardContent>
                <IonCardContent>{item.numero}</IonCardContent>
              </IonCardContent>
              </IonItem>
              </IonCard>
            )
          })}
      </IonContent>
    </IonPage>
    );
  }
  
};

export default Avions;
