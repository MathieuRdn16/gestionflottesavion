import {
  IonContent,
  IonHeader,
  IonItem,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import React from "react";
import {addImage} from "../Model/Convertion";
import { useParams } from 'react-router';


const Sary: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Test Image</IonTitle>
        </IonToolbar>
      </IonHeader>
        <IonContent>
          <form>
            <IonItem>
                <input type="file" accept="imqge/*" name="image" onChange={(e: any) => addImage(e,"")} />
            </IonItem>
          </form>
        </IonContent>
    </IonPage>
  );
};

export default Sary;
