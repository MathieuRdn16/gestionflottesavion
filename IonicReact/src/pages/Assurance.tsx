import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar,IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle,IonButtons,IonMenuButton } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import { useParams } from 'react-router';

import useData from '../components/GetData';

const Assurance: React.FC = () => {
  const {mois}=useParams<{ mois: string;}>();
  var url="http://localhost:8080/assurances/"+mois.toString();
  const {data,error}=useData(url);
  if(!data){
    return <h1>Chargement....</h1>
  }else{
    return (<IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Assurance expiré dans {mois} mois</IonTitle>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Assurance expiré dans {mois} mois</IonTitle>
          </IonToolbar>
        </IonHeader>
          {data.map(item => {
            return (
              <IonCard>
              <IonCardContent>
                <IonCardTitle>{item.avion.numero}</IonCardTitle>
                <IonCardSubtitle>{item.avion.categorie.categorie} {item.avion.nom}</IonCardSubtitle>               
                <IonCardContent><strong>Date Payement : </strong>{item.payement}</IonCardContent>
                <IonCardContent><strong>Date Expiration :</strong> {item.expiration}</IonCardContent>
              </IonCardContent>
              </IonCard>
            )
          })}
      </IonContent>
    </IonPage>
    );
  }
  
};

export default Assurance;
