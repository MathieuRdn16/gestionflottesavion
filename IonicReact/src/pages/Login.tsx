import { IonButton, IonCheckbox, IonCol, IonContent, IonHeader, IonIcon, IonInput, IonItem, IonLabel, IonPage, IonRow, IonTitle, IonToolbar,IonButtons,IonMenuButton } from '@ionic/react';
import { Console } from 'console';
import { useParams } from 'react-router';
import './Login.css';
import UseData from '../components/GetLogin';





const Login: React.FC = () => {
  const {idav}=useParams<{ idav: string;}>();
  var mail="";
  var mdp="";
  function connect(){
    UseData('http://localhost:8080/admins',mail,mdp,idav);
  
  }
  
  return (
    <IonPage>
      <IonHeader >
        <IonToolbar>
          <IonTitle>Se connecter</IonTitle>
          <IonButtons slot="start">
            <IonMenuButton />
          </IonButtons>
        </IonToolbar>
  <div className="box"> 
  <div className="titre"> 
  </div>
  <br></br>
  <br></br>
  <IonItem>
    <IonLabel position="floating">Email</IonLabel>
    <IonInput type="text" onIonChange={(e)  => mail=e.detail.value!}/>   
  </IonItem>
  <IonItem>
    <IonLabel position="floating">Mot de passe</IonLabel>
    <IonInput type="password" onIonChange={(e)  => mdp=e.detail.value!}/>
  </IonItem>
  <br></br>
  <br></br>
  <IonButton className="ion-margin-top" type="submit" expand="block" onClick={connect}>
    Login
  </IonButton>
  </div>
  </IonHeader>
    </IonPage>
  );
};

export default Login;
