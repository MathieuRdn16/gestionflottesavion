import React, {useEffect,useState } from "react";
import axios from "axios";

function UseData(url: any, login: String ,mdp:String ,idAv:string) {
    axios
            .post(url,{"nom": login,"mdp": mdp})
            .then((response)=>{
                console.log(response.data);
                if(response.data.data!=null) {
                    localStorage.setItem("token",response.data.data.valeur); 
                    window.location.href="/deplacement/"+idAv;     
                }
            })
            .catch((error)=>{
            })
    
}

export default UseData;