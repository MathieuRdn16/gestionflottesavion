import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet, setupIonicReact,IonSplitPane } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Assurance from './pages/Assurance';
import Login from './pages/Login';
import Menu from './components/Menu';
import Deco from './pages/Login';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import Avions from './pages/ListeAvion';
import Deplacements from './pages/AvionDetail';
import Sary from './pages/Sary';

setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
    <IonSplitPane contentId="main">
          <Menu />
          <IonRouterOutlet id="main">
        <Route exact path="/Avion">
          <Avions />
        </Route>
        <Route exact path="/">
          <Redirect to="/Avion" />
        </Route>
        <Route exact path="/assurance/:mois" >
          <Assurance />
        </Route>
        <Route exact path="/deplacement/:idAv" >
          <Deplacements />
        </Route>
        <Route exact path="/login/:idav" >
          <Login />
        </Route>
        <Route exact path="/changeprofil/:idav" >
          <Sary />
        </Route>
      </IonRouterOutlet>
      </IonSplitPane>
    </IonReactRouter>
  </IonApp>
);

export default App;
